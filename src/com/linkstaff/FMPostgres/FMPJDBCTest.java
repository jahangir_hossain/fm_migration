package com.linkstaff.FMPostgres;

import javax.print.DocFlavor;
import java.sql.*;

public class FMPJDBCTest {

    public static void main(String[] args) {
        // register the JDBC client driver
        try {
            Driver d = (Driver) Class.forName("com.filemaker.jdbc.Driver").newInstance();
        } catch (Exception e) {
            System.out.println(e);
        }
        // establish a connection to FileMaker
        Connection con = null;
        try {
            con = DriverManager.getConnection("jdbc:filemaker://localhost/新病院リスト", "admin", "izo4oi@lt556");
            System.out.println("-----connected with filemaker----------------");

            //display all the table of this database.
            displayFMDBTable(con);

            //display a specific table info

            //insert data into database.
            insertDataFromFMToRelationalDB(con);

        } catch (Exception e) {
            System.out.println(e);
        }
        // get connection warnings
        SQLWarning warning = null;
        try {
            warning = con.getWarnings();
            if (warning == null) {
                System.out.println("No warnings");
                return;
            }
            while (warning != null) {
                System.out.println("Warning: " + warning);
                warning = warning.getNextWarning();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public static void displayFMDBTable(Connection con) throws SQLException {

        System.out.println("----------------displayFMDBTable()------------------------");
        //---- Start: consoling database tables------------
        DatabaseMetaData metaData = con.getMetaData();
        String[] types = {"TABLE"};
        //Retrieving the columns in the database
        ResultSet tables = metaData.getTables(null, null, "%", types);
        int counter = 1;
        while (tables.next()) {
            String tableName = tables.getString("TABLE_NAME");
            tableName = tableName.replace('★', 's');
            tableName = tableName.replace('2', '_');
            System.out.println("___Creating_Table____: " + tableName);
            displayTableInfoDetails(con, tableName);
        }
        //----End: consoling database tables------------
        System.out.println("-------------------------------------------------------------");
        tables.close();
    }

    public static void displayTableInfoDetails(Connection con, String tableName) throws SQLException {

        System.out.println("----------------displayTableInfoDetails()------------------------");

        PreparedStatement ps = null;

        if (tableName.equals("hospital") || tableName.equals("temp_hospital") || tableName.equals("content_history")||tableName.equals("from_address_to_zip_code")) {
            ps = con.prepareStatement("select * from "+tableName);
            ResultSet rs = ps.executeQuery();
            ResultSetMetaData rsmd = rs.getMetaData();

            System.out.println("----------Table Name: " + tableName + "--------------------");
            int totalColumn = +rsmd.getColumnCount();
            System.out.println("Total columns: " + totalColumn);
            //病院情報
            String createHospitalTableQuery = "Create Table IF NOT EXISTS " + tableName + "(id int primary key,";
            for (int i = 1; i <= totalColumn; i++) {

//                System.out.print("Column No: " + i + "<--||-->");
                String columnName = rsmd.getColumnName(i);
                columnName = columnName.replace('-', '_');
                columnName = columnName.replace(':', '_');
                columnName = columnName.replace('%', '_');
                columnName = columnName.replace('[', '_');
                columnName = columnName.replace(']', '_');


                String columnType = rsmd.getColumnTypeName(i);
//                System.out.print("Column Name: " + columnName);
//                System.out.println(" <---||---> Column Type Namecolumn: " + columnType);

                if (i == totalColumn) {
                    createHospitalTableQuery = createHospitalTableQuery + " " + columnName;
                    createHospitalTableQuery = createHospitalTableQuery + " " + columnType + ")";
                } else {
                    createHospitalTableQuery = createHospitalTableQuery + " " + columnName;
                    createHospitalTableQuery = createHospitalTableQuery + " " + columnType + ",";
                }


            }
            Connection postgresConn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/fmhospital", "postgres", "postgres");
//        PreparedStatement createTable = postgresConn.prepareStatement("Create Table IF NOT EXISTS 病院情報(id int primary key, 病院名 varchar, 病院体系判定 text) ");
            System.out.println("##QueryString: " + createHospitalTableQuery);


            PreparedStatement createTable = postgresConn.prepareStatement(createHospitalTableQuery);


            createTable.executeUpdate();
            createTable.close();

        }


        System.out.println("-------------------------------------------------------------");

    }

    public static void insertDataFromFMToRelationalDB(Connection con) {

        System.out.println("----------------insertDataFromFMToRelationalDB()------------------------");

        try {
//            con.setAutoCommit(false);
            Statement st = con.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY,
                    java.sql.ResultSet.CONCUR_READ_ONLY);
            st.setFetchSize(50);
            ResultSet rs = st.executeQuery("SELECT TEL, FAX FROM hospital where TEL IS NOT NULL");
            System.out.println("Fetched Size: " + rs.getFetchSize());
            int counter = 1;
            while (rs.next()) {
//                System.out.println(".............................................................................");
//                System.out.println("Row: " + counter++ + "=>" + rs.getString(1) + "<-|->" + rs.getString(2));

            }

        } catch (SQLException e) {
            System.out.println("Exception: " + e);
        }


        System.out.println("-------------------------------------------------------------");


    }
}
